const express = require("express");

var cors = require('cors')

var app = express()
app.use(cors());
app.use('/*', function (req, res, next) {
    
    var url = req.query.url;
     return getScript(url).then((response)=> {
        //res = response;
        console.log("Resonse in the server: ", response);
        return res.status(200).send(response);
    }, (error)=> {
        console.log("Got error: " + error.message);
        return "Error";
    });
    // axios.get(url).then(function(response) {
    //     console.log("Axios response: ", stringify(response));
    // })
    // var options = {
    //     host: url,
    //     port: 80,
    //     path: ''
    //   };
    //   console.log("URL: ", url);
    //   https.get(url, function(res) {
    //     var data = '';
    //     res.on('data', function (chunk) {
    //         data += chunk;
    //     });
    //    res.on('end', function () {
    //       console.log("Data is: ", data);
    //       res.json({result: data});
    //         });
            
    //     //console.log("Got response: " + res);
    //     return data;
    //   }).on('error', function(e) {
    //     console.log("Got error: " + e.message);
    //     return "Error";
    //   });
  });
app.listen(5000);

var http = require('http');
var https = require('https');
const { stringify } = require("querystring");
const { default: axios } = require("axios");

// function getContent() {
//     var options = {
//         host: 'google.com',
//         path: '/'
//     }
//     var request = http.request(options, function (res) {
//         var data = '';
//         res.on('data', function (chunk) {
//             data += chunk;
//         });
//         res.on('end', function () {
//             console.log(data);
    
//         });
//     });
//     request.on('error', function (e) {
//         console.log(e.message);
//     });
//     request.end();
// }



const getScript = (url) => {
    return new Promise((resolve, reject) => {
        const http      = require('http'),
              https     = require('https');

        let client = http;

        if (url.toString().indexOf("https") === 0) {
            client = https;
        }

        client.get(url, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                console.log("Data is: ", data);
                resolve(data);
            });

        }).on("error", (err) => {
            reject(err);
        });
    });
};

(async (url) => {
    console.log(await getScript(url));
})('https://sidanmor.com/');