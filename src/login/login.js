import { Component } from "react";
import React, { useState } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./login.css";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  state = {
    userName: '',
    password: ''
  }

  handleSubmit(event){
    localStorage.setItem("Username", this.state.userName);
    event.preventDefault();
    this.props.history.push('/');
  }

  render() {
    return (
      <div className="Login">
        <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>Username</Form.Label>
          <Form.Control value={this.state.userName} onChange={e => this.setState({ userName: e.target.value })} placeholder="Username" />
        </Form.Group>
      
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control value={this.state.password} onChange={e => this.setState({ password: e.target.value })} type="password" placeholder="Password" />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={this.handleSubmit}>
          Submit
        </Button>
      </Form>
      </div>
    );
  }
}


export default Login;