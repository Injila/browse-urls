import React, { Component } from "react";
import { Navbar, Form, FormControl, FormGroup, Button} from 'react-bootstrap';
import './home.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';

class Home extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    username : localStorage.getItem("Username"),
    val1: "",
    val2: "",
    content1: ""
  };

  logout = () => {
    localStorage.setItem("Username", "");
    this.props.history.push('/login');
  }

  loadData = (paraURL, whichURL) => {
    var url = 'http://localhost:5000/api?url='+paraURL;
    fetch(url)
      .then(function (res) {
        if(res.ok){
          return res.text();
        }
        throw new Error('Error message.');
      })
      .then(function (data) {
        if(whichURL == "URL1") {
          this.setState({ content1: data });
        document.getElementById("web1").innerHTML = this.state.content1;
        }
        else {
          this.setState({ content2: data });
        document.getElementById("web2").innerHTML = this.state.content2;
        }
        
      }.bind(this))
      .catch(function (err) {
        console.log("failed to load ", url, err.message);
      });
  }

  getURL = (whichURL) => {
    if(whichURL == "URL1") {
      this.loadData(this.state.val1, whichURL);
    }
    else {
      this.loadData(this.state.val2, whichURL);
    }
  };

  render() {
    return (
      <>
      <div className="container-fullwidth">
      <div className="container-fullwidth header-style">
      <Navbar className="header-style" bg="light">
        <div className="col-sm-3">
        <span>{this.state.username}</span>
        </div>
        
        <Form inline>
          <FormGroup>
            <div className="col-sm-6">
            <FormControl type="text" placeholder="URL 1" className="mr-sm-2" value={this.state.val1} onChange={e => this.setState({ val1: e.target.value })}/>
            <Button className="btn btn-secondary" onClick={() => this.getURL("URL1")}>Find</Button>
            </div>
            <div className="col-sm-6">
            <FormControl type="text" placeholder="URL 2" className="mr-sm-2" value={this.state.val2} onChange={e => this.setState({ val2: e.target.value })}/>
            <Button className="btn btn-secondary" onClick={() => this.getURL("URL2")}>Find</Button>
            </div> 
          </FormGroup> 
        </Form>
        <div className="col-sm-3">
        <Button className="btn" onClick={this.logout}>Logout</Button>
        </div>
        
    </Navbar>
          </div>
      
      <div className="row margin-top-150">
      <div id="web1" className="col-md-6 webpage1-style">
      </div>
      <div  id="web2" className="col-md-6 webpage2-style">
      </div>
      </div>
    </div>
      </>
    );
  }
  
}

export default Home;
