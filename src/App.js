import React, { Component } from "react";
import './App.css';
import Home from './home/home';
import Login from './login/login';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Redirect } from 'react-router';


class ProtectedRoute extends Component {
  state = {
    username: localStorage.getItem("Username") || ''
  }
  render() {
    const { component: Component, ...props } = this.props

    return (
      <Route 
        {...props} 
        render={props => (
          this.state.username.length > 0 ?
            <Component {...props} /> :
            <Redirect to='/login' />
        )} 
      />
    )
  }
}


class App extends Component {
 state = {
   username: ''
 }
  constructor(props) {
    super(props);
  }
  requireAuth(nextState, replace, next) {
    debugger;
    let username = localStorage.getItem("Username");
    
    if (username == null || username.length == 0) {
      this.props.history.push('/login');
      replace({
        pathname: "/login",
        state: {nextPathname: nextState.location.pathname}
      });
    }
    else {
      this.setState({username: username});
    }
    next();
  }

  render() {
    return (
      <div className="App">
        <Router>
        <Switch>
        <Route path='/login' component={Login} />
        <ProtectedRoute path='/' component={Home} />
        </Switch>
        </Router>
      </div>
    );
  }
  
}

export default App;
